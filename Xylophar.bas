''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' Xylophar - a gopherd written in FreeBasic (https://freebasic.net/)
'' Written in 2020 by Nathaniel Leveck (leveck@leveck.us) gopher://1436.ninja
'' See the LICENSE file for licensing information.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#Include "dir.bi"
dim shared as String dirfiles, selector, filetemp, typ, server, port, gopherdir, query
dim shared as Boolean cgi

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' Change the three lines below for your server, server should be your FQDN as it
'' will be used in automatically generated directory listings.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
server = "localhost"
port = "70"
gopherdir = "/var/gopher"

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' SUBs, ETC
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub replace(byref t as string, byref i as string, byref s as string, byval a as integer = 1)
  'string replace since FreeBasic does not natively have this
  dim as Integer p , li, ls
  p= instr(a, t, i)
  li = len(i) : ls = len(s)
  if li = ls then li = 0
  while p
    if li then t = left(t, p - 1) & s & mid(t, p + li) else mid(t, p) = S
    p = instr(p + ls, t, i)
  wend
End Sub

Sub killit()
  ' Noticed there was a tendency towards stuck pids
  ' let's get rid of that...
  shell "kill -9 $$"
  end
End Sub

Sub redir()
  dim msg as string
  print "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>"
  print "<html>"
  print "  <head>"
  print "    <meta HTTP-EQUIV='REFRESH' content='0; url=" & selector & "'>"
  print "    <title>Gopher to HTTP Redirect</title>"
  print "  </head>"
  print "  <body>"
  print "    <h1>Leaving Gopher</h1>"
  print "    <p>You should be redirected automatically to " & selector & ".</p>"
  print "    <p>If you weren't, click to be redirected: <a href='" & selector & "'>" & selector & "</a></p>"
  print "  </body>"
  print "</html>"
  killit
End Sub

Sub determineTyp(byref filename as String, byref typ as string)
  'determine the type of file we are dealing with
  dim as string ext
  typ = ""
  cgi = false
  ext = lcase(mid(right(filename, 4), instr(right(filename, 4), "."), len(right(filename, 4))))
  'Add / modify file extensions below and their mapping to gopher file types
  select case ext
    case ".cgi"
      'files with .cgi extension are executed by the shell and treated as type 1
      'can be any executable file but should output gophermap formatted text
      typ = "1" : cgi = true
    case ".map"
      'files ending in .map are treated as gophermaps
      typ = "1"
    case ".pdf"
      typ = "P"
    case ".gif"
      typ = "g"
    case ".jpg",".jpe",".png",".bmp",".pcx","jpeg"
      typ = "I"
    case ".zip",".sit",".bin",".tar",".gz",".7z",".rar",".tgz",".xz",".pdb",".prc",".bz",".z"
      typ = "9"
    case ".htm","html"
      typ = "h"
    case else
      typ = "0"
  end select
End Sub

Sub PrintFolder(Path As String, ListType as Integer)
  'Print a directory listing
  Dim As String filename
  ChDir(Path)
  filename = Dir("*", ListType)
  Do While Len( filename ) > 0
    'Print Path & filename
    if filename = "gophermap" or filename = "gophermap.cgi" then
      'Found a gophermap in this directory!
      dirfiles = ""
      selector &= "/" & filename
      exit sub
    end if
    'skipping our README.nfo file...
    if filename <> "README.nfo" then
      if len(dirfiles) > 0 then
        dirfiles &= chr(13) & chr(10)
      end if
      if ListType = fbDirectory then
        dirfiles &= "1" & filename & chr(9) & mid(Path, 12, len(Path))
        dirfiles &= "/" & filename & "/" & chr(9) & server & chr(9) & port
      else
        determineTyp(filename, typ)
        dirfiles &= typ & filename & chr(9) & mid(Path, 12, len(Path))
        dirfiles &= "/" & filename & chr(9) & server & chr(9) & port
      end if
    end if
    filename = Dir( )
  Loop
End Sub

Sub gopherize(txt as string)
  'Make arbitrary text into gophermap'd text.
  dim tmp as string
  txt = "i" & txt & chr(9) & "ERR" & chr(9) & "err.host" & chr(9) & "0"
  print txt
End Sub

Sub readme()
  'If a file named README.nfo exists in a directory listing, then the contents
  'will be printed at the top of that directory listing.
  dim as string tmp, tmp2, tmp3
  tmp = selector & "/README.nfo"
  replace(tmp,"//","/")
  open tmp for input as #2
  do until eof(2)
    line input #2, tmp2
    gopherize(tmp2)
  loop
  close #2
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' MAIN SECTION
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Get the selector from the gopher client
input "", selector
if instr(selector, chr(9)) then
  query = mid(selector, instr(selector, chr(9)) + 1)
  selector = mid(selector, 1, instr(selector, chr(9)) - 1)
end if

if left(selector, 3) = "URL" then
  replace(selector, "URL:", "")
  replace(selector, "URL%3a", "")
  redir
end if

if left(selector, 1) = "/" then
  selector = gopherdir & selector
else
  selector = gopherdir & "/" & selector
end if

' come back here if a gophermap is found
sel:

replace(selector, "//", "/")

if dir(selector) <> "" then
  determineTyp(selector, typ)
  if typ = "0" or typ = "1" and cgi = false then
    open selector for input as #1
    do until eof(1)
      line input #1, filetemp
      print filetemp
    loop
    close #1
    'the final dot causes feeds not to parse, this is a workaround
    if right(selector,4) <> "atom" and right(selector,3) <> "rss" then
      print "." & chr(13)
    end if
    killit
  elseif typ = "1" and cgi = true then
    'This enables psuedo-CGI support...
    'parameter is passed to CGI as $2
    'this is done to preserve compatibility
    'with another gopherd
    shell selector & " dummy " & query
    print chr(13)
    print "." & chr(13)
    killit
  else
    'Got rid of the nasty pile of calls
    'to shell, but this one line solves a
    'big problem.
    'TODO - eliminate the shell call
    shell "curl -sq file://" & selector
    killit
  end if
else
  PrintFolder selector, fbDirectory
  PrintFolder selector, 0
  if dirfiles = "" then goto sel
  replace(dirfiles, "//", "/")
  readme
  print dirfiles
  print "." & chr(13)
  killit
end if
