#
# Makefile
# leveck, 2020-01-15 21:47
#

clean:
	rm Xylophar

install:
	fbc Xylophar.bas
	cp Xylophar /usr/local/bin
	cp gopher /etc/xinetd.d/
	cp Xylophar.1 /usr/local/share/man/man1/

uninstall:
	rm /usr/local/bin/Xylophar
	rm /etc/xinetd.d/gopher
	rm /usr/local/share/man/man1/Xylophar.1

# vim:ft=make
#
