# Xylophar
A lightweight (68kb - 77kb depending on platform) gopherd written in [FreeBasic][1]. It has been tested on Gentoo Linux running on AMD64 and Raspbian on a Raspberry Pi 3b+.

## Reporting Bugs
Should you find an issue, drop the author an email: xylophargopherd@1436.ninja

## Configuration
Xylophar is configured in the sourcecode. There are three variables to change:

1) server -- the FQDN of your server
2) port -- usually port 70, but you do you
3) gopherdir -- defaults to /var/gopher, but can be where ever you'd like

## Installation
Modify the source as detailed above. Ensure you have the FreeBasic compiler (fbc), and curl (used to assist with serving binary files) installed on your server, then

```
sudo make install
```

The binary will be installed to /usr/local/bin and a service file will be copied to /etc/xinetd.d. You will then need to restart xinetd as per your OS. If you use a different super-server (IE something other than xinetd), then you will need to edit the Makefile to point the service file to the correct location. In this case, the service file (gopher) may need to be modified as well.

At this point files in /var/gopher (or where you set your gopherdir) will start being hosted on port 70. Congrats on your gopherhole!

## Uninstallation

```
sudo make uninstall
```

Will remove the files copied in installation.

## Directory Listings
Xylophar will generate a directory listing for you if there is no gophermap (or gophermap.cgi) in the directory. If you put a file named README.nfo in a directory, the contents of that file will be displayed at the top of the directory listing. README.nfo will be *gopherized* for you, and should just be in plaintext.

## Special File Types
Files ending in .map will be linked as a directory (type 1) in directory listings. Files ending in .cgi will be handed to the shell and executed. These will be linked to as type 1 in automatic directory listings. Any executable (script or binary) will work, but should output valid gophermap syntax. Parameters passed via type 7 need to be handled by your script as $2 (the second parameter). README.nfo is detailed in the prior section.

## File Types
File type autodetection is done by file extension. The existing ones can be modified, or added to in the source code.

## Additional Help
The information in this README is also in the man file.

```
man Xylophar
```
## See it in Action

Point your client (such as [Chæphron](https://gitlab.com/leveck/chaephron)) to gopher://1436.ninja to see this server in action (running on a Raspberry Pi 3b+). This gopherhole also displays via proxy at https://leveck.us.

[1]: https://www.freebasic.net/
